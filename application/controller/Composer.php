<?php

namespace application\controller;

use GuzzleHttp\Client;

class Composer
{
    const UPDATE_API = 'https://packagist.org/api/update-package';

    /**
     * @param \Slim\Http\Request $request
     * @param \Slim\Http\Response $response
     * @param $args
     *
     * @return mixed
     */
    public function publish($request, $response, $args)
    {
        $url = sprintf('%s?username=%s&apiToken=%s', self::UPDATE_API,getenv('composer_username'), getenv('composer_token'));
        echo $url;
        $params = $request->getParams();
        $result = ['code'=>1, 'msg'=>'error'];
        if (empty($params)) {
            $response->getBody()->write(json_encode($result, JSON_UNESCAPED_UNICODE));

            return $response;
        }
        $repository = getenv('git_repository');
        $git_http_url = getenv('git_http_url');
        if (empty($params[$repository][$git_http_url])) {
            $result['msg'] = '未找到git地址';
            $response->getBody()->write(json_encode($result, JSON_UNESCAPED_UNICODE));
        } else {
            $client = new Client();
            $res = $client->request('POST', $url, [
                'json' => [
                    'repository' => [
                        'url' => $params[$repository][$git_http_url]
                    ]
                ]
            ]);
            $content = $res->getBody()->getContents();
            //error_log(var_export($content, true), 3, __DIR__.'/composer.log');
            $result['msg'] = 'request';
            $result['data'] = $content;
            $response->getBody()->write(json_encode($result, JSON_UNESCAPED_UNICODE));
        }

        return $response;
    }
}