<?php

namespace application\middleware;

class Auth
{
    /**
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface|mixed
     */
    public function __invoke($request, $response, $next)
    {
        if (!($token = $request->getParam('token'))) {
            $response->getBody()->write(json_encode(['code'=>0, 'msg'=>'Token empty']));
        } elseif (!self::check($token)) {
            $response->getBody()->write(json_encode(['code'=>0, 'msg'=>'Token invalid']));
        } else {
            $response = $next($request, $response);
        }

        return $response;
    }

    public static function check($token)
    {
        $auth_app_id = getenv('auth_app_id');
        $auth_app_key = getenv('auth_app_key');
        if ($token !== md5(sha1($auth_app_id).sha1($auth_app_key))) {
            return false;
        }

        return true;
    }
}