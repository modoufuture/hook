<?php

require '../vendor/autoload.php';

$app = new Slim\App();

$dotenv = new Dotenv\Dotenv(dirname(__DIR__));
$dotenv->load();

$app->get('/', function($request, $response, $args){
    return 'web hook';
});
$app->post('/composer', '\\application\\controller\\Composer:publish')->add(\application\middleware\Auth::class);

$app->run();